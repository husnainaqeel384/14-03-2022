const express = require('express')
const router = express.Router();
const upload = require('../middleware/upload.file')
const controller = require('../controller/file.controller')

// const upload_image = upload.single('profile')
// router.route('/upload').post(upload_image, file_upload);
router.post('/upload', upload.single('profile'), controller.IMAGE);
module.exports = router
