const express = require('express')
const app = express()
const bodyparser = require('body-parser')
const dotenv = require('dotenv')
dotenv.config()

app.use(bodyparser.urlencoded({ extended: false }))
app.use(bodyparser.json());


const routes = require('./routes/file.router')

app.use('/v1', routes);


app.listen(process.env.PORT, () => {
    console.log(`Server is running on ${process.env.PORT}`)
})


